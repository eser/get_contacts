require "#{Rails.root}/lib/src/google_contact_fetcher"

class ContactsController < ApplicationController

  after_action :set_tokens, only: :get_tokens

  def index
    @page = (params[:page] || 1).to_i - 1
    @contacts = if session[:access_token]
                  @fetcher.access_token = session[:access_token]
                  @fetcher.fetch_contacts.in_groups_of(30, false)
                else
                  []
                end
  end

  def get_tokens
    if params[:code]
      @fetcher.code = params[:code]
      @fetcher.get_tokens
    end
    redirect_to root_path
  end

  def oauth
    redirect_to(@fetcher.auth.authorization_uri.to_s)
  end

  private

  def set_tokens
    session[:refresh_token] = @fetcher.refresh_token
    session[:access_token] = @fetcher.access_token
  end

end


class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception

  before_action :set_content_fetcher

  private

  def set_content_fetcher
    @fetcher ||= GoogleContactFetcher.new
  end

end

require 'signet/oauth_2/client'

class ClientIdMissing < StandardError; end
class ClientSecretMissing < StandardError; end
class ClientAddressMissing < StandardError; end

class GoogleContactFetcher

  def initialize(params={})
    raise ClientIdMissing if DEFAULT_CONFIGS["CLIENT_ID"].blank?
    raise ClientSecretMissing if DEFAULT_CONFIGS["CLIENT_SECRET"].blank?
    raise ClientAddressMissing if DEFAULT_CONFIGS["CLIENT_ADDRESS"].blank?
    @client_id = DEFAULT_CONFIGS["CLIENT_ID"]
    @client_secret = DEFAULT_CONFIGS["CLIENT_SECRET"]
    @client_address = DEFAULT_CONFIGS["CLIENT_ADDRESS"]
    @refresh_token = nil
    @access_token = nil
    @token_type = nil
    @token_expires_in = nil
    @code = nil
    @client = nil
  end

  def refresh_token
    @refresh_token
  end

  def access_token
    @access_token
  end

  def access_token=(access_token)
    @access_token = access_token
  end

  def code=(code)
    @code = code
  end

  def auth
    @client = Signet::OAuth2::Client.new(
      :authorization_uri => 'https://accounts.google.com/o/oauth2/auth',
      :token_credential_uri =>  'https://www.googleapis.com/oauth2/v3/token',
      :client_id => @client_id,
      :client_secret => @client_secret,
      :prompt => 'consent',
      :scope => 'https://www.google.com/m8/feeds/',
      :redirect_uri => "#{@client_address}/get_token"
    )
  end

  def get_tokens
    conn = Faraday.new(:url => 'https://www.googleapis.com')
    response = conn.post('/oauth2/v4/token', {
      :code => @code,
      :redirect_uri => "#{@client_address}/get_token",
      :client_id => @client_id,
      :client_secret => @client_secret,
      :grant_type => 'authorization_code'
    })
    resp_body = JSON.parse(response.body)
    @access_token = resp_body["access_token"]
    @refresh_token = resp_body["refresh_token"]
    @token_type = resp_body["token_type"]
    @token_expires_in = resp_body["expires_in"]
    self
  end

  def fetch_contacts
    conn = Faraday.new(url: "https://www.google.com")
    conn.authorization(:Bearer, @access_token)
    response = conn.get do |req|
      req.url '/m8/feeds/contacts/default/full/?max-results=1000&alt=json'
      req.headers['Content-Type'] = 'application/atom+xml'
      req.headers['Gdata-version'] = '3.0'
      req.headers['Content-length'] = '0'
    end
    format_contact_json(response.body)
  end

  private

  def format_contact_json(unformatted_contacts)
    contact_json = JSON.parse(unformatted_contacts)
    contact_json.dig("feed", "entry")
      .select{|entry| entry.dig("title", "$t").present? && entry["gd$email"].present? }
      .map{ |entity| [
        entity.dig("title", "$t"),
        entity["gd$email"].map{|address_list| address_list["address"]}.join(",")
      ]}
      .sort {|a, b| a[0] <=> b[0] }
  end

end


Rails.application.routes.draw do

  root to: 'contacts#index'

  get '/index' => 'contacts#index'

  get '/get_token' => 'contacts#get_tokens'

  get '/oauth' => 'contacts#oauth'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

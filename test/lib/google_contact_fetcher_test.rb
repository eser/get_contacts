require "#{Rails.root}/lib/src/google_contact_fetcher"
require 'minitest/autorun'
require 'test_helper'

describe GoogleContactFetcher do

  before do
    COPY_DEFAULT_CONFIGS ||= YAML::load(ERB.new(File.read(File.join(Rails.root, 'config', 'defaults', [Rails.env, 'yml.erb'].join('.')))).result(binding))
  end

  describe "when initialize without required parameters" do
    before do
      DEFAULT_CONFIGS["CLIENT_ID"] = nil
      DEFAULT_CONFIGS["CLIENT_SECRET"] = nil
      DEFAULT_CONFIGS["CLIENT_ADDRESS"] = nil
    end

    it "must raised an error if CLIENT_ID environment varible is missing" do
      DEFAULT_CONFIGS["CLIENT_ID"] = nil
      DEFAULT_CONFIGS["CLIENT_SECRET"] = "TEST"
      assert_raises ClientIdMissing do
        GoogleContactFetcher.new
      end
    end

    it "must raised an error if CLIENT_SECRET environment varible is missing" do
      DEFAULT_CONFIGS["CLIENT_ID"] = "TEST"
      DEFAULT_CONFIGS["CLIENT_SECRET"] = nil
      assert_raises ClientSecretMissing do
        GoogleContactFetcher.new
      end
    end

    it "must raised an error if CLIENT_ADDRESS environment varible is missing" do
      DEFAULT_CONFIGS["CLIENT_ID"] = "TEST"
      DEFAULT_CONFIGS["CLIENT_SECRET"] = "TEST"
      assert_raises ClientAddressMissing do
        GoogleContactFetcher.new
      end
    end
  end

  it 'auth method have to return a Signet Oath object' do
    DEFAULT_CONFIGS.merge!(COPY_DEFAULT_CONFIGS)
    fetcher = GoogleContactFetcher.new
    auth_obj = fetcher.auth
    assert(auth_obj.kind_of?(Signet::OAuth2::Client), true)
  end

end


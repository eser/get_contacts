require 'test_helper'

class ContactsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get contacts_index_url
    assert_response :success
  end

  test "should get get" do
    get contacts_get_url
    assert_response :success
  end

  test "should get oauth" do
    get contacts_oauth_url
    assert_response :success
  end

end

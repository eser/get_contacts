# Get Contacts - Get Contacts from Gmail Account #

Get Contacts application brings your first 1000 contact from Google Contacts and paginates them.

This application created with Rails 5.1.4 and deployed on Heroku with Docker.

You can reach the application using this link: https://protected-shelf-75969.herokuapp.com/

## Requirements

Application requires `config/defaults/test.yml.erb` and `config/defaults/development.yml.erb` files for running on development mode and executing tests.

The yml files have to include correct values with following parameters.

```
CLIENT_ADDRESS: 'http://localhost:3000'
CLIENT_ID: xxxxxxxxx
CLIENT_SECRET: xxxxxxxxx
```
> Heroku client and Docker packages have to be preinstalled!

## Deployment ##

If you wanted to deploy this application to heroku, you would follow this easy steps:

Execute this commands in your command line:

1. heroku plugins:install heroku-container-registry
2. heroku container:login
3. git clone eser@bitbucket.org:eser/get_contacts.git
4. cd ffpl
5. heroku create
6. heroku container:push web
7. heroku open

That is it!


## Development ##
1. git clone eser@bitbucket.org:eser/get_contacts.git
2. cd get_contacts
3. bundle install
4. cp config/defaults/production.yml.erb config/defaults/development.yml.erb (you have to enter correct credentials for Google Oauth)
5. rails s
6. Go to browser and enter http://locahost:3000

## Tests ##

You can execute tests for this application. These are the steps for you have to follow:

1. Go to application directory through terminal application
2. bundle install 
3. rake test test/lib/google_contact_fetcher_test.rb

## Questions for the client

* Some contacts do not have name. What are we going to do with them? (We are ignoring now.)
* Google does not allow to access all contact by default. We can allow the visitors for the maximum contact count. (Default 1000 for Get Contact)
* The application sorts contacts according to their name, it can do this by email
* The application has any authentication mechanism or account feature. We need this for saving user data. (Also, this will drop our Google API budget and will increase our application.)

## TODO
* Write documentation
* Write much more test
* Make pages more attractive
* Renew authentication token automatically or send visitors to authentication page if token expired

